<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /**
     * @var
     */
    private $pass_hasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->pass_hasher = $passwordHasher;
    }


    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        $userOne = new User;
        $userOne
            ->setFirstname("Jean")
            ->setLastname("Bon")
            ->setEmail("jeanbon@mail.yep")
            ->setRole("ROLE_ADMIN")
            ->setPassword($this->pass_hasher->hashPassword($userOne, "admin"));
        $manager->persist($userOne);

        $userTwo = new User;
        $userTwo
            ->setFirstname("Paul")
            ->setLastname("Ochon")
            ->setEmail("paulochon@mail.yep")
            ->setRole("ROLE_ADMIN")
            ->setPassword($this->pass_hasher->hashPassword($userTwo, "admin"));
        $manager->persist($userTwo);

        $manager->flush();
    }
}
