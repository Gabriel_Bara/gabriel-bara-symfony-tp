<?php

namespace App\Form;

use App\Entity\Rental;
use App\Entity\Reservation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('dateStart', DateType::class, [
                'label' => "Date de début de séjour :",
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd'
            ])
            ->add('dateEnd', DateType::class, [
                'label' => "Date de fin de séjour :",
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd'
            ])
            // ->add('poolAccessA', null, ["label" => "Accès piscine adulte :"])
            // ->add('poolAccessC', null, ["label" => "Accès piscine enfant :"])
            ->add('user', UserType::class)
            ->add('rental', RentalType::class)
            ->add('submit', SubmitType::class, ['label' => 'Enregistrer']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
