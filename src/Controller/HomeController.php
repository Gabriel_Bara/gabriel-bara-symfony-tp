<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;


class HomeController extends AbstractController
{

    /**
     * @Route("/", name="app_home")
     */
    public function home()
    {
        return $this->render('front/home.html.twig', []);
    }

    /**
     * @Route("/about", name="app_about")
     */
    public function about()
    {
        return $this->render('front/about.html.twig', []);
    }

    /**
     * @Route("/rentals", name="app_rentals")
     */
    public function rentals()
    {
        return $this->render('front/rentals.html.twig', []);
    }

    /**
     * @Route("/login", name="login")
     * @return void
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastIdent = $authenticationUtils->getLastUsername();
        return $this->render('front/login.html.twig', [
            'lastIdent' => $lastIdent,
            'error' => $error
        ]);
    }
}
