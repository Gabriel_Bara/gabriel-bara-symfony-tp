<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Reservation;
use App\Entity\User;
use App\Form\ContactType;
use App\Form\ReservationType;
use App\Form\UserType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ReservationController extends AbstractController
{    
    // private $resRepo;

    // public function __construct(ReservationsRepository $resRepository) 
    // {
    //     $this->resRepo = $resRepository;
    // }


    // /**
    //  * @Route("/addContact", name="app_addContact")
    //  */
    // public function addContact(ManagerRegistry $manager, Request $request) 
    // {
    //     $contact = new Contact();
    //     $form = $this->createForm(ContactType::class, $contact);
    //     $form->handleRequest($request);
    //     $entityManager = $manager->getManager();

    //     if ($form->isSubmitted()) {
    //         $entityManager->persist($contact);
    //         $entityManager->flush();
    //         return $this->redirectToRoute('app_addUser');
    //     }

    //     return $this->render('front/reservation/add_contact.html.twig',[
    //         "form" => $form->createView()
    //     ]);
    // }

    // /**
    //  * @Route("addUser", name="app_addUser")
    //  */
    // public function addUser(ManagerRegistry $manager, Request $request) 
    // {
    //     $user = new User();
    //     $form = $this->createForm(UserType::class, $user);
    //     $form->handleRequest($request);
    //     $entityManager = $manager->getManager();

    //     if ($form->isSubmitted()) {
    //         $entityManager->persist($user);
    //         $entityManager->flush();
    //         return $this->redirectToRoute('app_addRes');
    //     }

    //     return $this->render('front/reservation/add_user.html.twig',[
    //         "form" => $form->createView()
    //     ]);
    // }

    /**
     * @Route("/addRes", name="app_addRes")
     */
    public function addReservation(ManagerRegistry $manager, Request $request) 
    {
        $reservation = new Reservation();
        $form = $this->createForm(ReservationType::class, $reservation);
        $form->handleRequest($request);
        $entityManager = $manager->getManager();

        if ($form->isSubmitted()) {
            $entityManager->persist($reservation);
            $entityManager->flush();
            return $this->redirectToRoute('app_home');
        }

        return $this->render('front/reservation/add_reservation.html.twig',[
            "addResForm" => $form->createView()
        ]);
    }
}