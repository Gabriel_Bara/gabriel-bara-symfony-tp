<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;



class AdminController extends AbstractController
{
    /**
     * @Route("/admin/home", name="dashboard", methods={"GET"})
     */
    public function adminHome()
    {
        return $this->render('admin/dashboard.html.twig', []);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
        return $this->redirectToRoute("app_home");
    }

    /**
     * @Route("/addOwner", name="app_addOwner")
     */
    public function addOwner()
    {
        return $this->render('admin/add_owner.html.twig', []);
    }

    /**
     * @Route("/addRental", name="app_addRental")
     */
    public function addRental()
    {
        return $this->render('admin/add_rental.html.twig', []);
    }
}
