<?php

namespace App\Entity;

use App\Repository\RentalsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RentalsRepository::class)
 */
class Rental
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=RentalType::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $rentalType;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRentalType(): ?RentalType
    {
        return $this->rentalType;
    }

    public function setRentalType(?RentalType $rentalType): self
    {
        $this->rentalType = $rentalType;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
