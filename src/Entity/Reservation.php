<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReservationRepository::class)
 */
class Reservation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Rental::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $rental;

    /**
     * @ORM\Column(type="date")
     */
    private $dateStart;

    /**
     * @ORM\Column(type="date")
     */
    private $dateEnd;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $poolAccessA;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $poolAccessC;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRental(): ?Rental
    {
        return $this->rental;
    }

    public function setRental(?Rental $rental): self
    {
        $this->rental = $rental;

        return $this;
    }

    public function getDateStart(): ?\DateTimeInterface
    {
        return $this->dateStart;
    }

    public function setDateStart(\DateTimeInterface $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getPoolAccessA(): ?int
    {
        return $this->poolAccessA;
    }

    public function setPoolAccessA(?int $poolAccessA): self
    {
        $this->poolAccessA = $poolAccessA;

        return $this;
    }

    public function getPoolAccessC(): ?int
    {
        return $this->poolAccessC;
    }

    public function setPoolAccessC(?int $poolAccessC): self
    {
        $this->poolAccessC = $poolAccessC;

        return $this;
    }
}
