<?php

namespace App\Entity;

use App\Repository\FacturationsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FacturationsRepository::class)
 */
class Facturation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $poolAccess;

    /**
     * @ORM\Column(type="integer")
     */
    private $reservationCost;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalPrice;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPoolAccess(): ?int
    {
        return $this->poolAccess;
    }

    public function setPoolAccess(int $poolAccess): self
    {
        $this->poolAccess = $poolAccess;

        return $this;
    }

    public function getReservationCost(): ?int
    {
        return $this->reservationCost;
    }

    public function setReservationCost(int $reservationCost): self
    {
        $this->reservationCost = $reservationCost;

        return $this;
    }

    public function getTotalPrice(): ?int
    {
        return $this->totalPrice;
    }

    public function setTotalPrice(int $totalPrice): self
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }
}
